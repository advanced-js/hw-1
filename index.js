"use strict";
/*1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.

Прототипне наслідування працює з даними типу "object". В кожного об'єкту є методи, які зберігаються у вбудованій властивості Prototype. Так ось, якщо новий об'єкт створюється на основі іншого об'єкта, то новий об'єкт успадковує ті методи, які є у батьківскому Prototype об'єкта. І таким чином, в нового об'єкта може не бути метода, а в батькіському об'єкті він є і даний метод буде доступним для нового об'єкта. Можна сказати, що новий об'єкт протопипно успадкував метод. Наприклад: 

class Car{
    constructor(brand, year){
        this.brand = brand;
        this.year = year;
    }
    getInfoCar(){
        return `Авто ${this.brand} ${this.year} року випуску`
    }
}

class MyCar extends Car{
    constructor(brand, year, color){
            super(brand, year);
            this.color = color;
    }
}

const toyota = new MyCar("toyota", 2022, "black");
Здавалося б, що наступна стрічка не виконається, адже в класі MyCar ми не робили метод getInfoCar. Але новий об'єкт прототипно успадковув метод від Car.
console.log(toyota.getInfoCar()); // Авто toyota 2022 року випуску


2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

Mетод super() у конструкторі нащадка викликається для того, щоб нащадок міг успадкувати та отримати ті властивості, які є у батьківського класа. Якщо взяти приклад вище, то бачимо, що завдяки методу super() клас MyCar отримує властивості brand, year від класа-батька Car.

*/

class Employee{
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name(){
        return this._name;
    }

    set name(value){
        this._name = value;
    }

    get salary(){
        return this._salary;
    }

    set salary(value){
        return this._salary = value;
    }

    get age(){
        return this._age;
    }

    set age(value){
         this._age = value;
    }

}


class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this.lang = lang;
    }
    get salary(){
        return this._salary *= 3;
    }
    set salary(value){
        return this._salary = value;
    }
}


// let employee = new Employee("Mary", 32, 2000);
// console.log(employee);
// console.log(employee.name);
// employee.name = "Ann";
// console.log(employee.name);
// console.log(employee);


// let programer1 = new Programmer("Alex", 25, 1000, ["en", "ru", "ukr"]);
// console.log(programer1);
// console.log(programer1.salary);
// programer1.salary = 2000;
// console.log(programer1.salary);
// console.log(programer1);


// let programer2 = new Programmer("Timur", 42, 5000, "ukr");
// console.log(programer2);
// console.log(programer2.salary);
// programer2.salary = 1000;
// console.log(programer2.salary);




let programer3 = new Programmer("Alisa", 20, 7000, "fr");
console.log(programer3);
programer3.salary = 5000;
console.log(programer3.salary);
programer3.age = 28;
programer3.name = "Alison";
console.log(programer3);

